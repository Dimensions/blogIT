<section class="pe-list" id="_category">
    <aside class="box-fl left">

        <div class="project-sort">
            <header class="box title vertical">
			<span class="box-fr">
				<svg width="5" height="10" viewBox="0 0 5 10" class="arrow-w">
                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#arrow-w"></use>
                </svg>
			</span>

                <div class="box-aw">
                    全部试题
                </div>
            </header>
            <div id="v-sort">
                <div class="menus">
                    <div class="item">
                        <a class="box menu vertical">
					<span class="box-fr ic">
						<svg width="5" height="10" viewBox="0 0 5 10" class="arrow-w">
                            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#arrow-w"></use>
                        </svg>

					</span>
                            <span class="box-aw name">编程语言</span>
                        </a>

                        <div class="drop">
                            <div class="box-aw sort-list">
                                <a href="/project/lang/19/java" title="Java">
                                    <span class="name">Java(1)</span>
                                    <span class="num"></span>
                                </a><a href="/project/lang/21/c" title="C/C++">
                                <span class="name">C/C++(1)</span>
                                <span class="num"></span>
                            </a><a href="/project/lang/341/objc" title="Objective-C">
                                <span class="name">Objective-C(1)</span>
                                <span class="num"></span>
                            </a><a href="/project/lang/22/php" title="PHP">
                                <span class="name">PHP(1)</span>
                                <span class="num"></span>
                            </a><a href="/project/lang/25/python" title="Python">
                                <span class="name">Python(1)</span>
                                <span class="num"></span>
                            </a><a href="/project/lang/26/ruby" title="Ruby">
                                <span class="name">Ruby(1)</span>
                                <span class="num"></span>
                            </a><a href="/project/lang/194/csharp" title="C#">
                                <span class="name">C#(1)</span>
                                <span class="num"></span>
                            </a><a href="/project/lang/20/dotnet" title=".NET">
                                <span class="name">.NET(1)</span>
                                <span class="num"></span>
                            </a><a href="/project/lang/358/go" title="Google Go">
                                <span class="name">Google Go(1)</span>
                                <span class="num"></span>
                            </a><a href="/project/lang/28/javascript" title="JavaScript">
                                <span class="name">JavaScript(1)</span>
                                <span class="num"></span>
                            </a><a href="/project/lang/269/html-css" title="HTML/CSS">
                                <span class="name">HTML/CSS(1)</span>
                                <span class="num"></span>
                            </a><a href="/project/lang/160/lua" title="Lua">
                                <span class="name">Lua(1)</span>
                                <span class="num"></span>
                            </a><a href="/project/lang/209/shell" title="SHELL">
                                <span class="name">SHELL(1)</span>
                                <span class="num"></span>
                            </a><a href="/project/lang/162/asm" title="汇编">
                                <span class="name">汇编(1)</span>
                                <span class="num"></span>
                            </a><a href="/project/lang/436/swift" title="Swift">
                                <span class="name">Swift(1)</span>
                                <span class="num"></span>
                            </a>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <a class="box menu vertical">
					<span class="box-fr ic">
						<svg width="5" height="10" viewBox="0 0 5 10" class="arrow-w">
                            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#arrow-w"></use>
                        </svg>

					</span>
                            <span class="box-aw name">数据库</span>
                        </a>

                        <div class="drop">
                            <div class="box-aw sort-list">
                                <a href="/project/tag/354/spdy-libs" title="MySQL ">
                                    <span class="name">MySQL </span>
                                    <span class="num">(13)</span>
                                </a><a href="/project/tag/355/html5" title="Oracle ">
                                <span class="name">Oracle </span>
                                <span class="num">(121)</span>
                            </a><a href="/project/tag/359/pjax" title="SQL Server ">
                                <span class="name">SQL Server </span>
                                <span class="num">(4)</span>
                            </a><a href="/project/tag/360/webapi" title="DB2 ">
                                <span class="name">DB2 </span>
                                <span class="num">(114)</span>
                            </a><a href="/project/tag/403/restful" title="Redis ">
                                <span class="name">Redis </span>
                                <span class="num">(80)</span>
                            </a><a href="/project/tag/405/responsive-framework" title="MongoDB ">
                                <span class="name">MongoDB </span>
                                <span class="num">(46)</span>
                            </a><a href="/project/tag/418/weixin" title="HBase ">
                                <span class="name">HBase </span>
                                <span class="num">(103)</span>
                            </a><a href="/project/tag/418/weixin" title="其他 ">
                                <span class="name">其他 </span>
                                <span class="num">(103)</span>
                            </a>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <a class="box menu vertical">
					<span class="box-fr ic">
						<svg width="5" height="10" viewBox="0 0 5 10" class="arrow-w">
                            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#arrow-w"></use>
                        </svg>

					</span>
                            <span class="box-aw name">操作系统</span>
                        </a>

                        <div class="drop">
                            <div class="box-aw sort-list">
                                <a href="/project/tag/342/android-ui" title="Windows ">
                                    <span class="name">Windows </span>
                                    <span class="num">(302)</span>
                                </a><a href="/project/tag/455/react-native" title="Linux ">
                                <span class="name">Linux </span>
                                <span class="num">(28)</span>
                            </a><a href="/project/tag/456/website-app" title="Unix ">
                                <span class="name">Unix </span>
                                <span class="num">(47)</span>
                            </a><a href="/project/tag/457/nativescript-plugins" title="Android ">
                                <span class="name">Android </span>
                                <span class="num">(18)</span>
                            </a><a href="/project/tag/336/iphone-dev-tools" title="Ios ">
                                <span class="name">Ios </span>
                                <span class="num">(20)</span>
                            </a><a href="/project/tag/337/wp7-dev-tools" title="Windows Phone ">
                                <span class="name">Windows Phone </span>
                                <span class="num">(22)</span>
                            </a>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <a class="box menu vertical">
					<span class="box-fr ic">
						<svg width="5" height="10" viewBox="0 0 5 10" class="arrow-w">
                            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#arrow-w"></use>
                        </svg>

					</span>
                            <span class="box-aw name">软件工程</span>
                        </a>

                        <div class="drop">
                            <div class="box-aw sort-list">
                                <a href="/project/tag/409/ios-activity" title="程序员 ">
                                    <span class="name">程序员 </span>
                                    <span class="num">(36)</span>
                                </a><a href="/project/tag/412/ios-pull-to-refresh" title="架构师 ">
                                <span class="name">架构师 </span>
                                <span class="num">(64)</span>
                            </a><a href="/project/tag/413/ios-menu" title="软件工程师 ">
                                <span class="name">软件工程师 </span>
                                <span class="num">(161)</span>
                            </a><a href="/project/tag/426/ios-gps" title="项目经理 ">
                                <span class="name">项目经理 </span>
                                <span class="num">(25)</span>
                            </a><a href="/project/tag/431/ios-form" title="通信工程师 ">
                                <span class="name">通信工程师 </span>
                                <span class="num">(27)</span>
                            </a>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <a class="box menu vertical">
					<span class="box-fr ic">
						<svg width="5" height="10" viewBox="0 0 5 10" class="arrow-w">
                            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#arrow-w"></use>
                        </svg>

					</span>
                            <span class="box-aw name">网络工程</span>
                        </a>

                        <div class="drop">
                            <div class="box-aw sort-list">
                                <a href="/project/tag/326/scripting" title="网络安全 ">
                                    <span class="name">网络安全 </span>
                                    <span class="num">(131)</span>
                                </a><a href="/project/tag/338/map" title="网络技术 ">
                                <span class="name">网络技术 </span>
                                <span class="num">(42)</span>
                            </a><a href="/project/tag/349/epub" title="网络管理员 ">
                                <span class="name">网络管理员 </span>
                                <span class="num">(8)</span>
                            </a><a href="/project/tag/392/uikit" title="网络工程师 ">
                                <span class="name">网络工程师 </span>
                                <span class="num">(126)</span>
                            </a><a href="/project/tag/407/code-generator" title="软件测试 ">
                                <span class="name">软件测试 </span>
                                <span class="num">(40)</span>
                            </a>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <a class="box menu vertical">
					<span class="box-fr ic">
						<svg width="5" height="10" viewBox="0 0 5 10" class="arrow-w">
                            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#arrow-w"></use>
                        </svg>

					</span>
                            <span class="box-aw name">其他技术</span>
                        </a>

                        <div class="drop">
                            <div class="box-aw sort-list">
                                <a href="/project/tag/317/decompiler" title="AJAX ">
                                    <span class="name">AJAX </span>
                                    <span class="num">(29)</span>
                                </a><a href="/project/tag/344/ci" title="EJB ">
                                <span class="name">EJB </span>
                                <span class="num">(34)</span>
                            </a><a href="/project/tag/357/sql-injector" title="Hibernate ">
                                <span class="name">Hibernate </span>
                                <span class="num">(19)</span>
                            </a><a href="/project/tag/398/git-tools" title="LoadRunner ">
                                <span class="name">LoadRunner </span>
                                <span class="num">(177)</span>
                            </a><a href="/project/tag/115/java-development-too" title="Mobile开发 ">
                                <span class="name">Mobile开发 </span>
                                <span class="num">(170)</span>
                            </a><a href="/project/tag/116/dotnet-development-t" title="QTP ">
                                <span class="name">QTP </span>
                                <span class="num">(50)</span>
                            </a><a href="/project/tag/117/php-development-tool" title="Mybatis ">
                                <span class="name">Mybatis </span>
                                <span class="num">(118)</span>
                            </a><a href="/project/tag/118/c-development-tools" title="Spring ">
                                <span class="name">Spring </span>
                                <span class="num">(104)</span>
                            </a><a href="/project/tag/119/ruby-development-too" title="SOA ">
                                <span class="name">SOA </span>
                                <span class="num">(78)</span>
                            </a><a href="/project/tag/120/python-development-t" title="Struts ">
                                <span class="name">Struts </span>
                                <span class="num">(124)</span>
                            </a><a href="/project/tag/121/perl-development-too" title="嵌入式开发 ">
                                <span class="name">嵌入式开发 </span>
                                <span class="num">(27)</span>
                            </a><a href="/project/tag/122/bugtracker" title="硬件 ">
                                <span class="name">硬件 </span>
                                <span class="num">(76)</span>
                            </a>
                            </div>
                        </div>
                    </div>
                    <div class="drop-mask" style="height: 202px;">
                    </div>
                </div>
            </div>
        </div>
        <template id="project-sort">
            <div class="menus" @mouseleave="clearDrop()" @mouseenter="countDown()">
                <div class="item" v-for="(index,item) in param.menu.items">
                    <a class="box menu vertical"
                       :class="{'active': active &amp;&amp; (my === index),'selected': item.active}"
                       @mouseleave.stop="action()" @mouseenter="showDrop(index)">
					<span class="box-fr ic">
						<svg width="5" height="10" viewBox="0 0 5 10" class="arrow-w">
                            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#arrow-w"></use>
                        </svg>

					</span>
                        <span class="box-aw name" v-text="item.name"></span>
                    </a>

                    <div class="drop" :class="{'open': active &amp;&amp; (my === index)}">
                        <div class="box-aw sort-list">
                            <a :href="child.href" v-for="child in item.childs" :title="child.name">
                                <span class="name" v-text="child.name"></span>
                                <span class="num" v-text="child.num"></span>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="drop-mask" :class="{'open': active &amp;&amp; (my !== index)}">

                </div>
            </div>
        </template>
        <div name="www_project_left" data-traceid="www_project_left_ad" data-tracepid="www_project_left">
            <p></p>
            <center>

            </center>
        </div>
    </aside>
    <div class="panel-wrapper" style="margin-top:30px;">
        <header class="panel-heading">
            <span class="title">公司招聘</span>
            <a href="/project/list?sort=time&amp;__from=plist-more" style=" float: right; ">更多</a>
        </header>
        <section class="panel-body">
            <div class="author-list">
                <div class="box vertical author">
                    <a href="https://my.oschina.net/barat" class="box-fl" title="阿里巴巴">
                        <img class="blog-author"
                             src="https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1493120750636&di=724e8b45013d799513d7ac61db1e57ae&imgtype=0&src=http%3A%2F%2Fwww.iaeweb.com%2Fpictures%2F2014%2F02%2FyhhkIk.png"
                             alt="阿里巴巴">
                    </a>

                    <div class="box-aw">
                        <a href="https://my.oschina.net/barat" class="name" title="阿里巴巴">阿里巴巴</a>
                        <div class="book">
                            <a href="https://www.oschina.net/p/oschina-wp7-app">
                                <span class="call">Java工程师</span>
                                <span class="num">(5)</span>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="box vertical author">
                    <a href="https://my.oschina.net/javayou" class="box-fl" title="腾讯">
                        <img class="blog-author"
                             src="https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1493120960721&di=f50ea4cc8a8c70824464ddfeb8e5477d&imgtype=jpg&src=http%3A%2F%2Fimg4.imgtn.bdimg.com%2Fit%2Fu%3D294831732%2C560060165%26fm%3D214%26gp%3D0.jpg" alt="腾讯">
                    </a>

                    <div class="box-aw">
                        <a href="https://my.oschina.net/javayou" class="name" title="腾讯">腾讯</a>

                        <div class="book">
                            <a href="https://www.oschina.net/p/py3cache">
                                <span class="call">Java工程师</span>
                                <span class="num">(5)</span>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="box vertical author">
                    <a href="https://my.oschina.net/sbz" class="box-fl" title="百度">
                        <img class="blog-author"
                             src="https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1493121051325&di=0494d82059d708c1fec80a66e940954b&imgtype=0&src=http%3A%2F%2Fcuimg.zuyushop.com%2FcuxiaoPic%2F201412%2F2014120020045241723.jpg" alt="百度">
                    </a>

                    <div class="box-aw">
                        <a href="https://my.oschina.net/sbz" class="name" title="百度">百度</a>

                        <div class="book">
                            <a href="https://www.oschina.net/p/zwall">
                                <span class="call">Java工程师</span>
                                <span class="num">(5)</span>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="box vertical author">
                    <a href="https://my.oschina.net/liygheart" class="box-fl" title="华为">
                        <img class="blog-author"
                             src="https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1493121108827&di=1ddec869fa4dc99b7176e456a0ede86a&imgtype=0&src=http%3A%2F%2Fpic21.photophoto.cn%2F20111122%2F0007019944803434_b.jpg" alt="华为">
                    </a>

                    <div class="box-aw">
                        <a href="https://my.oschina.net/liygheart" class="name" title="华为">华为</a>

                        <div class="book">
                            <a href="https://www.oschina.net/p/jfinalbbs">
                                <span class="call">Java工程师</span>
                                <span class="num">(5)</span>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="box vertical author">
                    <a href="https://my.oschina.net/xdev" class="box-fl" title="小米">
                        <img class="blog-author"
                             src="https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1493121186964&di=8c994fcd2f2bad644185be2f6003f6b0&imgtype=0&src=http%3A%2F%2Fww1.sinaimg.cn%2Flarge%2F86afb21etw1egp7l49flij20c80gcmy2.jpg" alt="小米">
                    </a>

                    <div class="box-aw">
                        <a href="https://my.oschina.net/xdev" class="name" title="xdev">小米</a>

                        <div class="book">
                            <a href="https://www.oschina.net/p/hbuilder">
                                <span class="call">Java工程师</span>
                                <span class="num">(5)</span>
                            </a>
                        </div>
                    </div>
                </div>

                <div class="box vertical author">
                    <a href="https://my.oschina.net/xdev" class="box-fl" title="京东">
                        <img class="blog-author"
                             src="https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1493121308706&di=d8e4e4aeae67b722691d167de8f95361&imgtype=0&src=http%3A%2F%2Fwww.lgstatic.com%2Fthumbnail_300x300%2Fimage1%2FM00%2F00%2F76%2FCgo8PFTUXamAG-mAAAC6GKBL05Q507.png" alt="京东">
                    </a>

                    <div class="box-aw">
                        <a href="https://my.oschina.net/xdev" class="name" title="xdev">京东</a>

                        <div class="book">
                            <a href="https://www.oschina.net/p/hbuilder">
                                <span class="call">Java工程师</span>
                                <span class="num">(5)</span>
                            </a>
                        </div>
                    </div>
                </div>

                <div class="box vertical author">
                    <a href="https://my.oschina.net/xdev" class="box-fl" title="美团">
                        <img class="blog-author"
                             src="https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1493121406491&di=4ddd418827655819e8e7b55c166b749b&imgtype=0&src=http%3A%2F%2Fimg.25pp.com%2Fuploadfile%2Fapp%2Ficon%2F20160411%2F1460343670420901.jpg" alt="美团">
                    </a>

                    <div class="box-aw">
                        <a href="https://my.oschina.net/xdev" class="name" title="xdev">美团</a>

                        <div class="book">
                            <a href="https://www.oschina.net/p/hbuilder">
                                <span class="call">Java工程师</span>
                                <span class="num">(5)</span>
                            </a>
                        </div>
                    </div>
                </div>

                <div class="box vertical author">
                    <a href="https://my.oschina.net/xdev" class="box-fl" title="魅力惠">
                        <img class="blog-author"
                             src="https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1493121469571&di=19ddc1526d7460d112ee0b5b5dc0221a&imgtype=0&src=http%3A%2F%2Ffile.digitaling.com%2FeImg%2Flogo%2F20140925%2F20140925212335_69291_320.png" alt="魅力惠">
                    </a>

                    <div class="box-aw">
                        <a href="https://my.oschina.net/xdev" class="name" title="xdev">魅力惠</a>

                        <div class="book">
                            <a href="https://www.oschina.net/p/hbuilder">
                                <span class="call">Java工程师</span>
                                <span class="num">(5)</span>
                            </a>
                        </div>
                    </div>
                </div>

            </div>
        </section>
    </div>
</section>
