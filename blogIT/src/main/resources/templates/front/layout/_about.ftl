<div id="wrapper">
    <div id="content">
        <h1>关于blogIT</h1>
        <div class="grid-16-8 clearfix">
            <div class="article">
                <span class="wrap"><h3>blogIT的来历</h3></span>
                <div>
                    　　<p style="text-indent:2em;">你经常对着电脑为了某个问题一筹莫展吗？或者头晕脑涨地一个人在各种弯道里走来走去？21世纪是一个创新与挑战并存的世纪，这也是互联网行业关键的一个世纪。“互联网+”给互联网企业跨界融合带来很多机会。与文化产业融合是腾讯长期深耕的主业，从游戏到视频、音乐、文学、动漫等领域均大有建树。面对向其他所有行业“加载”的机遇，腾讯将自己定位为连接工具，表示将依靠合作伙伴，主要通过微信、QQ等连接服务、内容和硬件设备。</p><br>
                        <p style="text-indent:2em;">blogIT形成了由开源软件库、代码分享、资讯、协作翻译、讨论区和博客等几大频道内容，为IT开发者提供了一个发现、使用、并交流开源技术的平台。blogIT建立大型综合性的云开发平台-中国源，为中国广大开发者提供团队协作、源码托管、代码质量分析、代码评审、测试、代码演示平台等功能。</p><br>
                        <p style="text-indent:2em;">我们一直不遗余力地推动国内开源软件的应用和发展，为本土开源能力的提高、开源生态环境的优化提供长期推进的平台,可以提供初级程序员工作就业面试以及职业规划等一条龙服务。</p><br>
                        <p style="text-indent:2em;">blogIT职业培训涵盖前端，后端，移动全品类。Web、Java、Python、Go、iOS、Android、PHP等各学科依据职业需求设计课程，根据个人学习计划提供视频、图文、答疑、一对一作业批改、推荐就业等服务。</p><br>
                </div>
                <span class="wrap"><h3>参与blogIT</h3></span>
                <div>
                    　　<p style="text-indent:2em;">blogIT没有编辑写手，没有特约文章，没有六百行的首页和跳动的最新专题。blogIT的藏书甚至没有强加给你的“标准分类”。这里所有的内容，分类，筛选，排序都由和你一样的成员产生和决定。给评论一个“有用”，它的排位会自动上升；贴“我女儿的最爱”给一本书，它会在整个网站的标签分类中出现。blogIT相信大众的力量，多数人的判断，和数字的智慧。通过网站幕后不断完善之中的算法，有序和有益的结构会从无数特异而可爱的个性中产生。</p>
                    　　<p style="text-indent:2em;">blogIT网站的每个开发管理者也都是blogIT每日的用户，分享着自己心爱的发现，也从你的参与中受益。我们鼓励你通过点击成员的名号或头像访问别人的个人主页，并充实自己的收藏或评论。这些是blogIT上最重要和有益的内容。</p>
                </div>
            </div>
            <div class="aside">
                <p class="pl2">&gt; <a href="https://blog.douban.com">blogIT blog</a></p>
                <p class="pl2">&gt; <a href="/jobs">加入blogIT团队</a></p>
                <p class="pl2">&gt; <a href="/about?policy=guideline">社区指导原则</a></p>
                <p class="pl2">&gt; <a href="/help/">帮助中心</a></p>
                <p class="pl2">&gt; <a href="/about?policy=privacy">隐私声明</a></p>
                <p class="pl2">&gt; <a href="/about?link=2us">链我们 link to us!</a></p>
            </div>
            <div class="extra">
            </div>
        </div>
    </div>
</div>